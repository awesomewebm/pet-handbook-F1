---
forceTheme: green
---
# PET Handbook 
Welcome to the Pinewood Emergency Team Handbook! This handbook outlines rules, frequently asked questions, our promotion system, and subdivision information.   
The purpose of PET is to keep players of Pinewood Builders Facilities safe.   
PET (Pinewood Emergency Team) is split into 3 subdivisions these being Fire, Hazmat, and Medical each ran by an assigned Chief.   
Any question regarding the following information can be sent to a Division Trainer, Chief, or Manager.  

## PET Leadership: 

- [Diddleshot](https://www.roblox.com/users/390939/profile): Owner of Pinewood Builders 
- [Csdi](https://www.roblox.com/users/33093423/profile): PET Manager 
 
## Current Team Chiefs:

- [AtomicMoosh](https://www.roblox.com/users/133233378/profile) - Medical Team Chief 
- [Neal_Forest](https://www.roblox.com/users/288096737/profile) - Hazmat Team Chief 
- [AnuCat](https://www.roblox.com/users/69576694/profile) - Fire Team Chief 


## General Rules
**If any of these rules are broken you will be given a consequence at the Trainer’s discretion**

### **(PET. 1)** Don't touch core controls  
PET may not touch core controls that heat or cool the core. E-Coolant may be used to save the core during a meltdown.

### **(PET. 2)** Don't cause disasters on-duty
PET may not cause disasters while On-duty.

### **(PET. 3)** No Group Weapons
PET may not carry any PBST or TMS weapons while on duty as PET, however gamepass or credit gear such as OP Weapons, the credit rifle/pistol, and rocket launcher are allowed for self defense.

### **(PET. 4)** No multiple ranking
PET Members may be in all teams if they wish, but a member in one team does not have authority over a member in another team. However a Division Trainer or PIA member can moderate any chat.

### **(PET. 5)** No being on-duty for multiple groups at once
PET may NOT be on-duty for more than one PET subgroup or another division such as PBST or TMS at the same time. 

### **(PET. 6)** Work together
All teams are allowed to cooperate with one another, but there will be NO conflicts between teams. If a conflict does start, it is to be reported to a Team Chief immediately.

### **(PET. 7)**  No shenanigans
PET is forbidden to start or spread fires or become mutants/zombies and hurting visitors/allies/PET members.

### **(PET. 8)** How to be on-duty
On-duty PET must have on the uniform (located in PET HQ) for the team they are on-duty for and have their rank tag set to PET (!setgroup pet). On-duty PET may only use the tools (located in PET HQ) for the division they are on-duty for (medical can’t use the fire team hose, hazmat can’t use the medical team medkit, etc).

### **(PET. 9)** No team switching during raids
During TMS raids PET members can not switch teams, they can however switch divisions.

### **(PET. 10)** Wear the correct uniform for your division
On-duty PET may only use the uniforms (located in PET HQ) for the division they are on-duty for. Custom or specialty uniforms are forbidden unless given direct permission by the host.

### **(PET. 11)** Only wear a uniform for what you're on-duty for
On-duty PET is not allowed to wear PET uniforms different to what they are on duty for. For example, If you were attending as Medical, you would not be able to use a Hazmat or Fire suit.

### **(PET. 12)** Have the ranktag enabled where you're on-duty
Kronos has updated and allows you to set your rank tag to specific-subdivisions. If you are to go on-duty for Fire, You should do !setgroup fire.

## Trainers and Chiefs Rules

### **(TC. 1)** Have a good training schedule
Trainers and Chiefs must figure out their time to host training so that it doesn't collide with other PET divisions.

### **(TC. 2)** 30 minutes between PET trainings
Trainers and Chiefs have to follow the “30 Minute Rule”, which states that there cannot be a training 30 minutes after or before a previous training. This only applies to PET trainings, PBST trainings do not comply with this rule.

### **(TC. 3)** Use the PET-Server
Trainers and Chiefs must utilize either the ```!petserver``` or a VIP server for trainings if at a PB facility, they may use a public server if doing a public server patrol. In rare instances, a joint activity may be hosted with PBST only by those who are both SD+ in security and a trainer in their PET division, regardless of the game. PET trainers not SD+ in security must be invited to co-host.

### **(TC. 4)** Use UTC as training time
Trainers must use UTC to schedule trainings. However, using EST/EDT as an extra display time is also allowed.

### **(TC. 5)** Exception to three rules
Trainers are exempt from (PET. 1) during trainings. They are also exempt from PET. 7 and (PET. 10) only when on duty for the team they are a trainer in. Team Chiefs do not have to wear a uniform no matter what division they are on-duty for.

::: warning NOTE:
***Breaking the below rules will result in permanent demotion from Division Trainer.***
:::

### **(TC. 6)** No admin abuse
Trainers may not abuse moderation commands in any way whether it be discord moderation commands or in-game moderation commands.

### **(TC. 6A)** Do not force people to Sector 5
Trainers may not intentionally log people in PBSTAC’s sector 5 or bring people to PBSTAC’s sector 5 even if they know the code.

### **(TC. 6B)** Do not use bypasses to obtain access game-altering tools
Trainers may never use bypasses to building tools or environment changing gears such as delete tools or paint buckets.

### **(TC. 6C)** Do not attempt to break into restricted areas with mod commands
Trainers may not use moderation commands to break into restricted areas such as PIA rooms or PBSTAC’s sector 5, Only enter Sector 5 if the code is known.

### **(TC. 6D)** Do not use mod commands in another Division Trainer's training unless given permission
Trainers may not use moderation commands during other trainer’s official events unless given direct permission from the host.
> ***Additional Notes***

PET HQ is located by the main transit station. From spawn go down the elevator and follow the indicator on the floor to the main transit station door. Once at the door to the transit station, follow the red indicator on the floor to PET HQ. PET HQ contains all team uniforms and equipment. 

Public fire extinguishers can be found in red givers around the map.

PB throwable medkits can be found at med stations around the map and require you to be in the Pinewood Builders Roblox group to obtain them.
> ***PET Honors***

***Throughout the month, Division Trainers and Team Chiefs can nominate people they believe are the best of the best in a specific division. All three divisions will have 1 honor member each month and will receive 12 points.***

### **(PET. H1)** Any Rank
Honors can be achieved at any rank.

### **(PET. H2)** Benefits
Honor members get access to a special channel with HRs and other honor members. They also get a special role in the PET Discord and In-Game on their ranktag.

### **(PET. H3)** Honors is earnable if you're not an HR in that division
Division Trainers can get PET Honors in other divisions they aren’t trainers in

### **(PET. H4)** Expiration
PET Honors only last for a total of 3 months. You can get honors again once it expires.

### **(PET. H5)** Consequences of bad actions
Honors can be revoked by a Team Chief if you’ve done something extremely disgraceful to the team you got honors in.

## Manager Rules
### **(MS. 1)** Host training for anyone
Managers are allowed to host trainings for all teams.

### **(MS. 2)** Exception to 1h rule
Managers also need to follow the 30 minute Training Policy, except they can override an SD PBST training. PBST is exempt from this rule. Only applies to PET.

## FAQ

 1. Are we allowed to carry weapons as PET?  
 You are allowed to carry Credit Weapons or any weapons from a gamepass.

 2. When are trainings hosted?  
 Trainings are hosted at random times, you can check the schedule in game at the PBDSF or run ``k!schedule`` pet in ``#bot-commands`` in the [PET Discord](https://discord.gg/t4KBPkM).

 3. Are we allowed to use the E-Coolant feature?  
Yes, you are allowed to use the E-Coolant if you must save the server. [Refer to this video by **Coasterteam** to see how it works.](https://www.youtube.com/watch?v=nskMAcMYE9M&feature=youtu.be)


::: warning NOTE:
These rules can be changed at any time. So please check every now and then for a update on the handbook.
:::


:::danger These rules are valid across all divisions!
If you want to see the rules per division, please select one of either divisions to see their rules.
:::
<center>
Select a subdivision
</center>
<div class="row">
  <div class="column">
    <a href="medic/">
      <center>
      <img src="https://t6.rbxcdn.com/b0bdf6de15beceba3b8a3502214d59ed"
        style="border-radius: 50%;">
      
   <p>Medical Team</p>
   </center>
    </a>
  </div>
  <div class="column">
    <a href="hazmat/">
      <center>
      <img src="https://t5.rbxcdn.com/677f0db1b57fedc0502bfeafd774acd6"
        style="border-radius: 50%;">
      
   <p>Hazmat Team</p>
   </center>
    </a>
  </div>
  <div class="column">
    <a href="fire/">
      <center>
      <img src="https://t2.rbxcdn.com/a796198d3e727715beb0158d5249bc45"
        style="border-radius: 50%;">
      
   <p>Fire Team</p>
      </center>
    </a>
  </div>
</div>
