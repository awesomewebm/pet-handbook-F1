---
forceTheme: blue
---
# Hazmat Handbook
<center>
Select a subdivision
</center>
<div class="row">
  <div class="column">
    <a href="../medic/">
      <center>
      <img src="https://t6.rbxcdn.com/b0bdf6de15beceba3b8a3502214d59ed"
        style="border-radius: 50%;">
      
   <p>Medical Team</p>
   </center>
    </a>
  </div>
  <div class="column">
    <a href="../hazmat/">
      <center>
      <img src="https://t5.rbxcdn.com/677f0db1b57fedc0502bfeafd774acd6"
        style="border-radius: 50%;">
      
   <p>Hazmat Team</p>
   </center>
    </a>
  </div>
  <div class="column">
    <a href="../fire/">
      <center>
      <img src="https://t2.rbxcdn.com/a796198d3e727715beb0158d5249bc45"
        style="border-radius: 50%;">
      
   <p>Fire Team</p>
      </center>
    </a>
  </div>
</div>

## Hazmat Team Rules
::: warning NOTE:
This is the handbook for the subsection of the Pinewood Emergency Team
**HAZMAT TEAM**

Please don't forget that is made for the hazmat team only, so no rules that are stated here apply on the other sub-divisions. 
Only if stated in their respective handbooks. 
:::



### **(HT. 1)** Wear suit at all times
Hazmat Team members must always wear the PET Hazmat suit at all times. This suit can be found at the PET HQ and allows for safe entrance of the core at temperatures over 3000. PET Hazmat may not use the MT’s “lunchbox” medkit or the FT’s hose. The PB throwable medkits and public fire extinguishers are free for use by all.

### **(HT. 2)** Be there for everyone
Hazmat Team members must always put their lives at risk during disasters such as the radiation leak or Neurotoxin gas. They must always be there to save the facility.

### **(HT. 3)** Access to fans in disaster
Hazmat Team members are permitted to use the fans during the neurotoxin disaster.

### **(HT. 4)** Only heat/cool core for save
Hazmat Team members are permitted to use any of the heating systems in the core. However, this is only to save the core. Hazmat Team members are NOT allowed to cause reactor meltdowns or Freezedowns.

### Hazmat Team Rank Requirements

Hazmat Trainee - Nothing, just join the group

Expert Hazmat - 30 Points

Hazmat CERT - 65 Points

Hazmat Specialist - 105 Points

Hazmat Lieutenant - 175 Points + Handpicked
